package org.example.tutorial;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Service1App {
	@Bean
	public ConnectionFactory connectionFactory() {
		return new CachingConnectionFactory("localhost");
	}

	@Bean
	public AmqpTemplate amqpTemplate() {
		var template = new RabbitTemplate(connectionFactory());
		template.setExchange("amq.fanout");

		return template;
	}

	public static void main(String[] args) {
		SpringApplication.run(Service1App.class, args);
	}
}
