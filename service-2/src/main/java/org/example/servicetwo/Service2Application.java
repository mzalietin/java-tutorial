package org.example.servicetwo;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@EnableRabbit
@SpringBootApplication
public class Service2Application {
	@Bean
	public ConnectionFactory connectionFactory() {
		return new CachingConnectionFactory("localhost");
	}

	//	@Bean
	//	public AmqpAdmin amqpAdmin() {
	//		return new RabbitAdmin(connectionFactory());
	//	}
	//
	//	@Bean
	//	public RabbitTemplate rabbitTemplate() {
	//		return new RabbitTemplate(connectionFactory());
	//	}
	//

	@Bean
	public Queue javaTutorial() {
		return new Queue("java-tutorial");
	}

	public static void main(String[] args) {
		SpringApplication.run(Service2Application.class, args);
	}
}
