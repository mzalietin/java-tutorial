package org.example.servicetwo;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitMqListener {

    @RabbitListener(queues = "java-tutorial")
    public void processQueue1(String message) {
        System.out.println("Received from 'java-tutorial' : " + message);
    }

    @RabbitListener(queues = "tutorial-fanout")
    public void processQueue2(String message) {
        System.out.println("Received from 'tutorial-fanout' : " + message);
    }
}
