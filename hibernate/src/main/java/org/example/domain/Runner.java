package org.example.domain;

import org.example.domain.entity.Engine;
import org.example.domain.entity.Passenger;
import org.example.domain.entity.Vehicle;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Set;

@SpringBootApplication
public class Runner {
    @Autowired
    private final SessionFactory sessionFactory;

    public Runner(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @PostConstruct
    private void run() {
        var session = sessionFactory.openSession();

        //Vehicle vehicle = session.get(Vehicle.class, 1L);
        var vehicle = new Vehicle();
        vehicle.setName("Honda Civic");
        vehicle.setId(1L);

        var engine = new Engine("v8", Short.valueOf("277"));
        vehicle.setEngine(engine);

        var passengers = Set.of(
                new Passenger("Maksim", "Zaletsin"),
                new Passenger("Dzmitry", "Bandalouski")
        );
        vehicle.setPassengers(passengers);

        session.beginTransaction();
        session.save(vehicle);
        session.getTransaction().commit();
        session.close();
    }

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }
}
