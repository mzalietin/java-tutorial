package org.example.domain.entity;

import javax.persistence.Embeddable;

@Embeddable
public class Engine {
    private String id;
    private Short horsePower;

    public Engine() {
    }

    public Engine(String id, Short horsePower) {
        this.id = id;
        this.horsePower = horsePower;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Short getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(Short horsePower) {
        this.horsePower = horsePower;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "id='" + id + '\'' +
                ", horsePower=" + horsePower +
                '}';
    }
}
