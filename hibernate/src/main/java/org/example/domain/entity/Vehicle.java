package org.example.domain.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "id", column = @Column(name = "engine_id")),
            @AttributeOverride(name = "horsePower", column = @Column(name = "engine_hp"))
    })
    private Engine engine;

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "passengers")
    private Collection<Passenger> passengers;

    public Vehicle() {
    }

    public Collection<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(Collection<Passenger> passengers) {
        this.passengers = passengers;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", engine=" + engine +
                ", passengers=" + passengers +
                '}';
    }
}
